<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'PostController@index');
Route::get('/create', 'PostController@create');
Route::post('/add', 'PostController@add');
Route::post('/update/{id}', 'PostController@update');
Route::post('/delete/{id}', 'PostController@delete');
