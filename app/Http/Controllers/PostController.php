<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('posts.home', ['posts' => $posts]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function add(Request $request)
    {
    	$this->validate($request, [
    		'title' => 'required',
    		'description' => 'required'
    	]);
		$posts = new Post;
		$posts->title = $request->input('title');
		$posts->description = $request->input('description');
		$posts->save();
		return redirect('/home')->with('info','Saved Successfully!');
    }
}
